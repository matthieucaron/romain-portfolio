import React from "react"
import styled from "styled-components"

const sidesText = () => (
  <React.Fragment>
    <LeftSide>copyright, 2019, Romain Richard</LeftSide>

    <RightSide>Development, Matthieu Caron</RightSide>
  </React.Fragment>
)

const LeftSide = styled.p`
  position: fixed;
  color: #9aa4f2;
  z-index: 100;
  transform: rotate(-90deg) translateY(-50%);
  font-size: 0.75rem;
  top: 50%;
  width: 320px;
  text-align: center;
  left: -95px;

  @media all and (max-width: 1024px) {
    display: none;
  }
`

const RightSide = styled.p`
  position: fixed;
  z-index: 100;
  transform: rotate(90deg) translateY(-50%);
  color: #9aa4f2;
  font-size: 0.75rem;
  top: 50%;
  width: 320px;
  text-align: center;
  right: -95px;

  @media all and (max-width: 1024px) {
    display: none;
  }
`

export default sidesText
