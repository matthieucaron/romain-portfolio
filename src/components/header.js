import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styled from "styled-components"

const Header = ({ siteTitle }) => (
  <HeaderPortfolio>
    <HeaderLink
      to="/"
      style={{
        textDecoration: `none`,
        marginLeft: "0",
        hasmargin: false,
        hasOpacity: false,
      }}
    >
      <Circle />
    </HeaderLink>
    <NavContainer>
      <HeaderLink
        to="/"
        activeClassName="active"
        style={{
          textDecoration: `none`,
        }}
      >
        HOME
      </HeaderLink>
      <HeaderLink
        to="/works"
        activeClassName="active"
        style={{
          textDecoration: `none`,
        }}
      >
        WORKS
      </HeaderLink>
      <HeaderLink
        to="/about"
        activeClassName="active"
        style={{
          textDecoration: `none`,
        }}
      >
        ABOUT
      </HeaderLink>
    </NavContainer>
  </HeaderPortfolio>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

const HeaderPortfolio = styled.header`
  position: fixed;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 30px 100px;

  @media all and (max-width: 420px) {
    padding: 30px 40px;
  }

  @media all and (max-width: 350px) {
    flex-direction: column;
    height: 150px;
  }
`

const NavContainer = styled.div`
  @media all and (max-width: 350px) {
    display: flex;
    justify-content: space-between;
    width: 100%;
  }
`

const HeaderLink = styled(style => <Link {...style} />)`
  font-size: 0.9rem;
  color: #9aa4f2;
  letter-spacing: 1px;
  opacity: ${style => (style.hasopacity ? "1" : "0.5")};
  margin-left: ${style => (style.hasmargin ? "0" : "30px")};

  @media all and (max-width: 350px) {
    margin-left: 0;
  }
`
const Circle = styled.div`
  width: 37px;
  height: 37px;
  border-radius: 50%;
  display: block;
  background-color: #9aa4f2;
`

export default Header
