import React, { Component } from "react"
import ScrollContainer from "react-indiana-drag-scroll"
import { Link } from "gatsby"
import styled, { keyframes } from "styled-components"

class Slider extends Component {
  render() {
    let array = [
      {
        name: "works-01",
        description: "Schmidt - Vertical Home",
        number: "01",
        year: "2018",
      },
      {
        name: "works-02",
        description: "Description",
        number: "02",
        year: "2019",
      },
      {
        name: "works-03",
        description: "Description",
        number: "03",
        year: "2020",
      },
      {
        name: "works-04",
        description: "Description",
        number: "04",
        year: "2017",
      },
      {
        name: "works-05",
        description: "Description",
        number: "05",
        year: "2017",
      },
      {
        name: "works-06",
        description: "Description",
        number: "06",
        year: "2017",
      },
    ]

    let images = array.map(works => {
      return (
        <React.Fragment>
          <WorksPreview to={works.name} className="works-preview">
            <Overlay className="overlay">
              <p className="number">{works.number}</p>
              <p className="subtitle">
                {works.description}
                <br />
                {works.year}
              </p>
            </Overlay>
            <img
              key={works.name}
              src={require(`../images/${works.name}.jpg`)}
              alt={works.description}
              className="img-responsive"
            />
          </WorksPreview>
          <Separator></Separator>
        </React.Fragment>
      )
    })

    return (
      <Scroll hideScrollbars={false} vertical={false}>
        <Flex className="flex">{images}</Flex>
      </Scroll>
    )
  }
}

const Width = keyframes`
  from {
    width: 0%;
  }
  to {
    opacity: 100%;
  }
`

const Overlay = styled.div`
  position: absolute;
  background-color: rgba(154, 164, 242, 0.5);
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  padding: 30px 20px;
  opacity: 0;
  transition: 0.5s ease;
  color: #000;

  .number {
    font-size: 3.1rem;
    font-family: "Futura-bold";
  }

  .subtitle {
    font-size: 0.9rem;
  }
`

const Scroll = styled(ScrollContainer)`
  width: 100%;
  animation: ${Width} 1s;
`

const Separator = styled.div`
  display: block;
  min-width: 15px;
  height: 30px;
  background-color: #fff;
`

const Flex = styled.div`
  display: flex;
  min-height: 100vh;
  align-items: center;
  img {
    display: block;
    min-width: 300px;
    margin-bottom: 0;
  }
`

const WorksPreview = styled(Link)`
  transform: scale(1);
  transition: 0.3s;
  position: relative;
  &:hover .overlay {
    opacity: 1;
    transition: 0.3s;
  }
  &:hover {
    transform: scale(1.03);
    transition: 0.3s;
    cursor: pointer;
    box-shadow: 0 24px 32px 0 rgba(0, 0, 0, 0.03),
      0 8px 32px 0 rgba(0, 0, 0, 0.06);
  }
  &:first-child {
    margin-left: 15%;
  }
`

export default Slider
