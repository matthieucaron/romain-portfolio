import React from "react"

import Layout from "../components/layout"
import Slider from "../components/slider"
import SEO from "../components/seo"

const SecondPage = () => (
  <Layout>
    <SEO title="Page two" />
    <Slider />
  </Layout>
)

export default SecondPage
