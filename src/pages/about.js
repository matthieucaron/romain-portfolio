import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import styled, { keyframes } from "styled-components"

const SecondPage = () => (
  <Layout>
    <SEO title="A propos" />
    <AboutContainer>
      <AboutText>
        My name is Romain Richard and I've been working in the{" "}
        <HighLight>digital field</HighLight>, and more precisely in{" "}
        <HighLight>creation</HighLight> for three years. <br />
        I'm currently working as a <HighLight>product designer</HighLight> at
        Red Pill.
      </AboutText>

      <AboutText>
        You can consult my <CustomLink to="/">book</CustomLink> and my{" "}
        <CustomLink to="/">resume</CustomLink> here.
        <br />
        You can also find me on <CustomLink to="/">dribbble</CustomLink>,{" "}
        <CustomLink to="/">instagram</CustomLink> and{" "}
        <CustomLink to="/">linkedin</CustomLink>.
      </AboutText>
    </AboutContainer>
  </Layout>
)

export const FadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`

const AboutContainer = styled.section`
  font-family: "Futura-book";
  padding: 100px 175px;
  min-height: 100vh;
  display: flex;
  align-content: center;
  justify-content: center;
  flex-direction: column;
  font-size: 1.6rem;
  line-height: normal;

  @media all and (max-width: 1024px) {
    padding: 100px 50px;
  }
  @media all and (max-width: 350px) {
    padding: 150px 50px;
  }
`

const AboutText = styled.p`
  font-family: "Futura-book";
  animation: ${FadeIn} 0.8s;
`

const HighLight = styled.strong`
  color: #9aa4f2;
`

const CustomLink = styled(Link)`
  color: #000;
  font-weight: 400;
  font-family: "Futura-book";
`

export default SecondPage
