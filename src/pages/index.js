import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import styled, { keyframes } from "styled-components"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <TextContainer>
      <TextHome>
        <ZIndex>Hi, I'm Romain,</ZIndex>
        <br /> <Bold>junior art director</Bold> <ZIndex>based in </ZIndex>
        <NoWrap>
          <Highlight>Paris</Highlight>.
        </NoWrap>
      </TextHome>
    </TextContainer>
  </Layout>
)

const ZIndex = styled.span`
  position: relative;
  z-index: 10;
`

const NoWrap = styled.span`
  white-space: nowrap;
`

const height = keyframes`
  from {
    width: 0;
  }

  to {
    right: 0px;
    width: 100%;
  }
`

const Highlight = styled.span`
  position: relative;
  z-index: 10;
  &:after {
    animation-duration: 0.5s;
    animation-name: ${height};
    animation-iteration-count: 1;
    animation-direction: alternate;
    position: absolute;
    content: "";
    right: 0px;
    height: 7px;
    width: 100%;
    background-color: #9aa4f2;
    bottom: 0;
  }
`

const slidein = keyframes`
  from {
    width: 0;
  }

  to {
    width: 150%;
  }
`

const Bold = styled.span`
  position: relative;
  z-index: 5;
  font-family: "Futura-bold";

  &:after {
    position: absolute;
    content: "";
    right: -10px;
    height: 55vh;
    min-height: 250px;
    width: 150%;
    background: #9aa4f2;
    top: 0;
    transform: translateY(-50%);
    opacity: 0.5;
    z-index: -1;
    animation-duration: 1s;
    animation-name: ${slidein};
    animation-iteration-count: 1;
    animation-direction: alternate;

    @media (max-width: 1000px) {
      top: 50%;
    }
    @media (max-width: 620px) {
      width: 500px;
      right: -125px;
      top: 50%;
      animation: none;
    }
  }
`

const fade = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`

const TextHome = styled.p`
  font-size: 3rem;
  animation-duration: 1s;
  animation-name: ${fade};
  animation-iteration-count: 1;
  animation-direction: alternate;
  line-height: 4.2rem;
  margin: 0 auto;

  @media all and (max-width: 500px) {
    font-size: 2.5rem;
  }
`

const TextContainer = styled.div`
  display: flex;
  align-items: center;
  margin: 0 auto;
  min-height: 100vh;

  @media all and (max-width: 1024px) {
    padding: 45px;
  }
`

export default IndexPage
